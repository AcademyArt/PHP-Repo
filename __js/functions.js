/* Documenting Notes: https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/javascript/ */

/**
 * Copyright (c) 2018
 *
 * This is where the global javascript functions will be kept for use in the website.
 *
 * @summary The site's javascript functions 
 * @author Andrew Pankow <apankow1234@gmail.com>
 */