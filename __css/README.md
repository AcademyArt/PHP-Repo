# PHP Repository

## Blank Starter

* [/](https://github.com/apankow1234/php_repo)
  * __css
    * site.css
  * [__js](../__js/)
    * functions.js
  * [__classes](../__classes/)
    * DatabaseObject.php
  * [__includes](../__includes/)
    * header.inc
    * footer.inc
    * sitewide_details.inc


site.css
```css
  /* More than just ASCII characters are allowed 
  * but can be further restricted or broadened  
  * by changing what's inside the quotation marks 
  */
  @charset "UTF-8";
```