# PHP Repository

## Blank Starter

* [/](https://github.com/apankow1234/php_repo)
  * [__css](../__css/)
    * site.css
  * [__js](../__js/)
    * functions.js
  * __classes
    * DatabaseObject.php
  * [__includes](../__includes/)
    * header.inc
    * footer.inc
    * sitewide_details.inc


### Available Objects

DatabaseObject.php
```php

	/**
	* DatabaseObject is an abstract ("template-like") object for database
	* interaction. 
	*
	* DatabaseObject is not intended to generalize all database 
	* functionality for all use-cases but rather meant to generalize
	* for website-specific needs. These include Querying, Inserting,
	* and Deleting data in predefined tables. Methods from this class
	* should be looked at as a weak API.
	*/
	abstract class DatabaseObject {...} 

	/**
	* MySQL_DBObject is an implementation of a DatabaseObject for MySQL
	*
	* Example usage:
	* $db = new MySQL_DBObject();
	*/
	class MySQL_DBObject extends DatabaseObject {...}

	/**
	* Oracle_DBObject is an implementation of a DatabaseObject for Oracle
	*
	* Example usage:
	* $db = new Oracle_DBObject();
	*/
	class Oracle_DBObject extends DatabaseObject {...}

	/**
	* SQLite_DBObject is an implementation of a DatabaseObject for SQLite
	*
	* Example usage:
	* $db = new SQLite_DBObject();
	*/
	class SQLite_DBObject extends DatabaseObject {...}

	/**
	* SQLServer_DBObject is an implementation of a DatabaseObject for SQL Server
	*
	* Example usage:
	* $db = new SQLServer_DBObject();
	*/
	class SQLServer_DBObject extends DatabaseObject {...}
```