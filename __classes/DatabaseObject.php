<?php
	
	/* Documenting Notes: https://docstore.mik.ua/orelly/webprog/pcook/ch21_09.htm */

	/**
	* DatabaseObject is an abstract ("template-like") object for database
	* interaction. 
	*
	* DatabaseObject is not intended to generalize all database 
	* functionality for all use-cases but rather meant to generalize
	* for website-specific needs. These include Querying, Inserting,
	* and Deleting data in predefined tables. Methods from this class
	* should be looked at as a weak API.
	*
	* @package  DatabaseObject
	* @author   Andrew Pankow
	* @version  $Revision: 1.0 $
	* @access   public
	* @see      http://andrewpankow.com/
	*/
	abstract class DatabaseObject
	{
		public function __construct()
		{

		}
		abstract function QueryDB()
		{
//	TODO(Andrew):	Document Method
			/* Will protect against Inserts and Deletes */
		} 
		abstract function GetByDBID()
		{
//	TODO(Andrew):	Document Method
			/* Advanced: Assumes Type 2 or 6 SCDs */
		} 
		abstract function GetByID()
		{
//	TODO(Andrew):	Document Method
		}
		abstract function FindInColumn()
		{
//	TODO(Andrew):	Document Method
		}
		abstract function FindInTable()
		{
//	TODO(Andrew):	Document Method
		}
		abstract function FindInDatabase()
		{
//	TODO(Andrew):	Document Method
		}
		abstract function InsertIntoDB()
		{
//	TODO(Andrew):	Document Method
		}
		abstract function DeleteFromDB()
		{
//	TODO(Andrew):	Document Method
		}

// TODO(andrew): Finish laying out the DatabaseObject functionality

	}



	/**
	* MySQL_DBObject is an implementation of a DatabaseObject for MySQL
	*
	* MySQL_DBObject allows the developer to use or switch to a MySQL 
	* database without needing to adjust the rest of the code throughout
	* the website. 
	*
	* Example usage:
	* $db = new MySQL_DBObject();
	*
	* @package  MySQL_DBObject
	* @author   Andrew Pankow
	* @version  $Revision: 1.0 $
	* @access   public
	* @see      http://andrewpankow.com/
	*/
	class MySQL_DBObject extends DatabaseObject
	{
		public function __construct()
		{
        	parent::__construct();
		}

// TODO(andrew): Add MySQL implementations of the DatabaseObject methods

	}



	/**
	* Oracle_DBObject is an implementation of a DatabaseObject for Oracle
	*
	* Oracle_DBObject allows the developer to use or switch to a Oracle 
	* database without needing to adjust the rest of the code throughout
	* the website. 
	*
	* Example usage:
	* $db = new Oracle_DBObject();
	*
	* @package  Oracle_DBObject
	* @author   Andrew Pankow
	* @version  $Revision: 1.0 $
	* @access   public
	* @see      http://andrewpankow.com/
	*/
	class Oracle_DBObject extends DatabaseObject
	{
		public function __construct()
		{
        	parent::__construct();
		}
		
// TODO(andrew): Add Oracle implementations of the DatabaseObject methods

	}



	/**
	* SQLite_DBObject is an implementation of a DatabaseObject for SQLite
	*
	* SQLite_DBObject allows the developer to use or switch to a SQLite 
	* database without needing to adjust the rest of the code throughout
	* the website. 
	*
	* Example usage:
	* $db = new SQLite_DBObject();
	*
	* @package  SQLite_DBObject
	* @author   Andrew Pankow
	* @version  $Revision: 1.0 $
	* @access   public
	* @see      http://andrewpankow.com/
	*/
	class SQLite_DBObject extends DatabaseObject
	{
		public function __construct()
		{
        	parent::__construct();
		}

// TODO(andrew): Add SQLite implementations of the DatabaseObject methods

	}



	/**
	* SQLServer_DBObject is an implementation of a DatabaseObject for SQL Server
	*
	* SQLServer_DBObject allows the developer to use or switch to a SQL Server 
	* database without needing to adjust the rest of the code throughout
	* the website. 
	*
	* Example usage:
	* $db = new SQLServer_DBObject();
	*
	* @package  SQLServer_DBObject
	* @author   Andrew Pankow
	* @version  $Revision: 1.0 $
	* @access   public
	* @see      http://andrewpankow.com/
	*/
	class SQLServer_DBObject extends DatabaseObject
	{
		public function __construct()
		{
        	parent::__construct();
		}

// TODO(andrew): Add SQL Server implementations of the DatabaseObject methods

	}

?>