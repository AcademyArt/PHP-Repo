# PHP Repository

## Blank Starter
* /
  * [__css](__css/)
    * site.css
  * [__js](__js/)
    * functions.js
  * [__classes](__classes/)
    * DatabaseObject.php
  * [__includes](__includes/)
    * header.inc
    * footer.inc
    * sitewide_details.inc

index.php
```php
<?php 
	$pageTitle       = "PHP Repository Homepage";
	$pageDescription = "This and other pages are and play host to PHP demonstrations."
	include( "__includes/sitewide_details.inc" );
	/* Adding Extra to the page */
	/* $allScripts     .= [ "script.js",              ]; */
	/* $allStyles      .= [ "stylesheet.css",         ]; */
	/* $allAuthors     .= [ "url" => "name/fullname", ]; */
	require_once( "__includes/header.inc" ); 
?>

<!-- Everything INSIDE the <body> Tags Goes Here (do not include the <body> tags) -->

<?php require_once( "__includes/footer.inc" ); ?>
```

