<?php 
	$pageTitle       = "Basic Page";
	$pageDescription = "This pages talks about a topic.";
	include( "__includes/sitewide_details.inc" );
	/* Adding Extra to the page */
	$allScripts     .= [ /* "script.js",              */ ];
	$allStyles      .= [ /* "stylesheet.css",         */ ];
	$allAuthors     .= [ /* "url" => "name/fullname", */ ];
	require_once( "__includes/header.inc" ); 
?>

<!-- Everything Inside the <body> Tags Goes Here -->

<?php require_once( "__includes/footer.inc" ); ?>