<?php
	
	if (!isset($pageTitle))       $pageTitle       = "Basic Page";
	if (!isset($pageDescription)) $pageDescription = "This pages talks about a topic.";
	if (!isset($allScripts))      $allScripts      = [ /* "script.js",              */ ];
	if (!isset($allStyles))       $allStyles       = [ /* "stylesheet.css",         */ ];
	if (!isset($allAuthors))      $allAuthors      = [ /* "url" => "name/fullname", */ ];

?><!Doctype html>
<html>
<head>
	<title><?php echo $pageTitle; ?></title>
	<meta name="description" content="<?php echo $pageDescription; ?>">
<?php foreach( $allScripts as $script ) : ?>
	<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php endforeach; ?>
<?php foreach( $allStyles as $style ) : ?>
	<link href="<?php echo $style; ?>" rel="stylesheet" type="text/css" >
<?php endforeach; ?>
<?php foreach( $allAuthors as $link => $author ) : ?>
	<meta name="author" content="<?php echo $author; ?>">
	<link rel=”author” href="<?php echo $link; ?>" />
<?php endforeach; ?>
</head>
<body>
	